*******************************************************
  README.txt for contaxe.module for Drupal
*******************************************************

The Contaxe module makes it easy to integrate Contaxe ads into drupal.

INSTALLATION:

1. Put the contaxe directory in the modules directory that makes
   the most sense for your site.  See http://drupal.org/node/70151 for tips
   on where to install contributed modules.
2. Enable contaxe via admin/build/modules.

OPTIONAL DEPENDENCIES

1. profile for revenue sharing
2. referral for sharing revenue with referrals
3. jquery_update, jquery_interface and interface_sortable for WhoSeesAds
   support.
4. search for integration of the intelligent search ad

TO DO:

1. Figure out why dragged items are shifted to the right in IE.
2. Clean up the Javascript.
3. Do we want to use hook_footer for sending out the Javascript, the way
   it's done in the messagefx module?

THANKS:

Thanks to the authors of the drupal modules jquery_update, jquery_interface,
interface_sortable, referral, adsense, adsense_injector and the WordPress
plugin WhoSeesAds.
